global sepia_filter

%define SIZE_PIXEL 3

%macro pack_to_xmm 2
    xorps  %1, %1
    pinsrb %1, [%2], 0
    pinsrb %1, [%2], 4
    pinsrb %1, [%2], 8
    cvtdq2ps %1, %1
%endmacro

%macro save 2          ; (source xmm, destination)
    cvtps2dq %1, %1             ; convert to doubleword
    pminud %1, xmm6             ; limit max color value
    
    pextrb [%2], %1, 0          ; extract colors from xmm to pointer
    pextrb [%2 + 1], %1, 4
    pextrb [%2 + 2], %1, 8
%endmacro

section .data

align 16
max_values_vector: dd 255, 255, 255, 0

align 16
b_vector: dd 0.131, 0.168, 0.189, 0.0
align 16
g_vector: dd 0.534, 0.686, 0.769, 0.0
align 16
r_vector: dd 0.272, 0.349, 0.393, 0.0

section .text

simd_filter:
    mov r12, rdi
    mov r13, rsi
    mov r14, rdx
    
    mov rax, r12
    mul r13
    mov rbx, PIXEL_SIZE
    mul rbx
    mov r15, rax
    
    movaps xmm3, [rel b_vector]
    movaps xmm4, [rel g_vector]
    movaps xmm5, [rel r_vector]
    movaps xmm6, [rel max_values_vector]

    xor rbx, rbx
    mov rbx, r14
.loop:
    xorps xmm0, xmm0
    xorps xmm1, xmm1
    xorps xmm2, xmm2
    pack_to_xmm xmm0, rbx
    pack_to_xmm xmm1, rbx + 1
    pack_to_xmm xmm2, rbx + 2
    mulps xmm0, xmm3
    mulps xmm1, xmm4
    mulps xmm2, xmm5
    addps xmm0, xmm1
    addps xmm0, xmm2
    save xmm0, rbx
    add rbx, PIXEL_SIZE
    sub r15, PIXEL_SIZE
    cmp r15, 0
    jl .end
    jmp .loop
.end:
    ret
