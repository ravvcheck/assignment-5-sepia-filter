//
// Created by RavvCheck1 on 30.10.2023.
//

#include <stdlib.h>


#include "image.h"

struct image *create_image_data(const uint32_t width, const uint32_t height) {
    struct image *img = malloc(sizeof(struct image));
    img->width = (uint32_t) width;
    img->height = (uint32_t) height;
    img->data = malloc(sizeof(struct pixel) * width * height);
    return img;
}

void free_image_data(struct image *img) {
    if (img->data == NULL) {
        return;
    }
    img->height = 0;
    img->width = 0;
    free(img->data);
}

uint32_t get_padding(uint32_t width) {
    return (4 - (width * sizeof(struct pixel)) % 4) % 4;
}
