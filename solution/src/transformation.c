//
// Created by RavvCheck1 on 30.10.2023.
//
#include "../include/sepia_filter.h"
#include "image.h"
#include "transformation.h"


unsigned char sat(uint64_t x) {
    if (x < 256) return x;
    return 255;
}

void sepia_one(struct pixel *const pixel) {

    float r = pixel->r;
    float g = pixel->g;
    float b = pixel->b;

    r = sat((uint64_t) ((r * 0.393f) + (g * 0.769f) + (b * 0.189f)));
    g = sat((uint64_t) ((r * 0.349f) + (g * 0.686f) + (b * 0.168f)));
    b = sat((uint64_t) ((r * 0.272f) + (g * 0.534f) + (b * 0.131f)));

    pixel->b = (uint8_t) b;
    pixel->g = (uint8_t) g;
    pixel->r = (uint8_t) r;
}

struct image *sepia_c_inplace(struct image *img) {
    uint32_t x, y;
    for (y = 0; y < img->height; y++) {
        for (x = 0; x < img->width; x++) {
            uint64_t index = y * img->width + x;
            sepia_one(&(img->data[index]));
        }
    }
    return img;
}

struct image *sepia_asm_filter(struct image *img_in) {
    sepia_filter(img_in->height, img_in->width, img_in->data);
    return img_in;
}
