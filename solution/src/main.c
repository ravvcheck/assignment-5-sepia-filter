#include "../include/transformation.h"
#include <bmp.h>
#include <in_out.h>
#include <stdlib.h>
#include <time.h>
#include <transformation.h>



#define WRITE_MODE "wb"
#define READ_MODE "rb"

int main(int argc, char **argv) {
    if (argc < 4) {
        fprintf(stderr, "Probably the number of arguments!\n");
        return 1;
    }

    FILE *in = open_file(argv[1], READ_MODE);
    if (in == NULL) {
        return 1;
    }


    struct image *img_in = NULL;
    if (from_bmp(in, &img_in) != READ_OK) {
        fprintf(stderr, "Error reading!\n");
        return 1;
    }

    fclose(in);

    struct image *img_out_c;
    struct image *img_out_asm;
    size_t iterations = 750;
    clock_t start_process_c = clock();
    for (size_t i = 0; i < iterations; i++) {
        img_out_c = sepia_c_inplace(img_in);
    }
    clock_t end_process_c = clock();
    clock_t start_process_asm = clock();
    for (size_t i = 0; i < iterations; i++) {
        img_out_asm = sepia_asm_filter(img_in);
    }
    clock_t end_process_asm = clock();

    double c_result = (double) (end_process_c - start_process_c) / CLOCKS_PER_SEC;
    double asm_result = (double) (end_process_asm - start_process_asm) / CLOCKS_PER_SEC;

    printf("%-11s %s \n\n", "Realization", "Result");
    printf("%-11s %.2fs \n", "\"SIMD\"", asm_result);
    printf("%-11s %.2fs \n", "\"NATIVE C\"", c_result);
    printf("\nTotal boost: %.2f\n", c_result / asm_result);

    FILE *out_1 = open_file(argv[2], WRITE_MODE);
    if (out_1 == NULL) {
        return 1;
    }
    FILE *out_2 = open_file(argv[3], WRITE_MODE);
    if (out_2 == NULL) {
        return 1;
    }

    if (to_bmp(out_1, img_out_c) != WRITE_OK) {
        fprintf(stderr, "Error writing!\n");
        return 1;
    }
    if (to_bmp(out_2, img_out_asm) != WRITE_OK) {
        fprintf(stderr, "Error writing!\n");
        return 1;
    }

    free_image_data(img_in);
    free(img_in);
    free_image_data(img_out_asm);
    free(img_out_asm);
    free_image_data(img_out_c);
    free(img_out_c);
    fclose(out_1);
    fclose(out_2);
    return 0;
}
