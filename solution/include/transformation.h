//
// Created by RavvCheck1 on 30.10.2023.
//

#ifndef IMAGE_TRANSFORMER_TRANSFORMATION_H
#define IMAGE_TRANSFORMER_TRANSFORMATION_H

#include <image.h>

unsigned char sat(uint64_t x);

void sepia_one(struct pixel* pixel);

struct image* sepia_c_inplace(struct image* img_in);

struct image* sepia_asm_filter(struct image* img_in);

#endif //IMAGE_TRANSFORMER_TRANSFORMATION_H
