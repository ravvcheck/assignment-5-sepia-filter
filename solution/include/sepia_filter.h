//
// Created by RavvCheck1 on 29.12.2023.
//

#ifndef SEPIA_FILTER_SEPIA_FILTER_H
#define SEPIA_FILTER_SEPIA_FILTER_H

#include "image.h"

void sepia_filter(uint64_t height, uint64_t width, struct pixel* data);

#endif //SEPIA_FILTER_SEPIA_FILTER_H
